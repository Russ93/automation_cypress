const _ = require('lodash');
const fs = require('fs');
const path = require('path');
const requireAll = require('require-all');

const fixtures = requireAll({
	dirname: path.join(__dirname, '/cypress/fixtureGenerators'),
	filter: /(.+)(?:\.js|\.json)$/,
	excludeDirs: /^\.(git|svn)$/,
});

_.forIn(fixtures, (v, k) => {
	const filename = path.join(__dirname, `/cypress/fixtures/${k}.json`);
	const data = JSON.stringify(v, null, 4);
	fs.writeFileSync(filename, data, 'utf-8');
});
