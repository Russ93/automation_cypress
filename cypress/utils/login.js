import userData from '../fixtures/userData';
module.exports = {
	main: userInstance(userData.main),
	friend: userInstance(userData.friend),
};

function userInstance(data) {
	return function (cy) {
		return cy.login(data);
	};
}
