import userData from '../../fixtures/userData';

Cypress.Commands.add('login', (userType) => {
	const data = userData[userType];
	const apiKey = Cypress.env('SSOP_KEY') || Cypress.env('API_KEY');
	const isLocal = Cypress.config('baseUrl').includes('localhost');

	cy.visit(`/api/passport/logout?api_key=${apiKey}`);
	cy.wait(1000);

	cy.get('[type="submit"]', { timeout: 5 * 60 * 1000 });
	cy.request(`/api/admin/system/verify?api_key=${apiKey}`);

	if (!data.registered && data.chargeover) return coRegister();
	if (!data.registered) publicRegister();
	cy.wait(1000);

	if (isLocal && data.chargeover) cy.request(`/api/organization/orgId?slug=${data.stamp}`);

	cy.request(`/api/admin/system/verify?api_key=${apiKey}`);
	cy.wait(1000);
	if (!isLocal) cy.visit(`${data.href}`);
	if (isLocal) cy.visit('/');

	cy.wait(1000);

	cy.get('[name="username"]').focus().invoke('val', data.email);
	cy.get('[name="password"]').focus().invoke('val', data.password);
	cy.wait(1000);

	cy.get('[type="submit"]', { timeout: 5 * 60 * 1000 }).click();
	cy.wait(1000);

	cy.get('[name="condensed"]');

	function coRegister() {
		cy.visit('/public/chargeover/signup');
		cy.wait(1000);
		// cy.pause();
		cy.get('[name="firstname"]').focus().invoke('val', data.firstname);
		cy.get('[name="lastname"]').focus().invoke('val', data.lastname);
		cy.get('[name="email"]').focus().invoke('val', data.email);
		cy.get('[name="password"]').focus().invoke('val', data.password);
		cy.contains('Continue').click();

		cy.get('button[name="Get Started for FREE"]').click();

		cy.get('[name="organization.name"]').focus().invoke('val', data.company);
		cy.get('[name="organization.slug"]').focus().invoke('val', data.stamp);

		cy.get('[value="creditcard"]').parent('label').click();
		cy.get('[name="number"]').focus().invoke('val', data.number);
		cy.get('[name="name"]').focus().invoke('val', data.fullname);
		cy.get('[name="expdate"]').focus().invoke('val', data.expdate);
		cy.get('[name="postcode"]').focus().invoke('val', data.postcode);

		cy.get('[name="company"]').focus().invoke('val', data.company);
		cy.get('[name="bill_addr1"]').focus().invoke('val', data.bill_addr1);
		cy.get('[name="bill_addr2"]').focus().invoke('val', data.bill_addr2);
		cy.get('[name="bill_city"]').focus().invoke('val', data.bill_city);
		cy.get('[name="bill_postcode"]').focus().invoke('val', data.bill_postcode);

		// cy.get('[name="bill_state"]').focus().invoke('val', data.bill_state);

		cy.get('[type="submit"]').click();
		cy.wait(1000);

		data.registered = true;
		userData.save();
	}

	function publicRegister() {
		cy.visit('/public/signup');
		cy.wait(1000);
		// cy.pause();
		cy.get('[name="firstname"]').focus().invoke('val', data.firstname);
		cy.get('[name="lastname"]').focus().invoke('val', data.lastname);
		cy.get('[name="email"]').focus().invoke('val', data.email);
		cy.get('[name="password"]').focus().invoke('val', data.password);
		cy.contains('Continue').click();

		cy.get('[name="Free Account"] button').click();
		cy.get('[name="free review container"] button').click();
		data.registered = true;
		userData.save();

		cy.url().should('include', '/payment-complete');
	}
});
