Cypress.Commands.add('getIframeBody', (selector) =>
	cy
		.get(selector)
		.its('0.contentDocument.body')
		.should('not.be.empty')
		// wraps "body" DOM element to allow
		// chaining more Cypress commands, like ".find(...)"
		// https://on.cypress.io/wrap
		.then(cy.wrap)
);
