Cypress.Commands.add('upload', { prevSubject: 'element' }, upload);
function upload(subject, file, fileName) {
	// we need access window to create a file below
	cy.window().then((window) => {
		// line below could maybe be refactored to make use of Cypress.Blob.base64StringToBlob, instead of this custom function.
		// inspired by @andygock, please refer to https://github.com/cypress-io/cypress/issues/170#issuecomment-389837191
		const blob = b64toBlob(file, '', 512);
		const defaultName = `${String(+new Date())}.webm`;

		// Please note that we need to create a file using window.File,
		// cypress overwrites File and this is not compatible with our change handlers in React Code
		const files = [new window.File([blob], fileName || defaultName)];
		const types = ['Files'];
		const event = { files, types, position: 'topLeft' };
		event.target = event;
		event.dataTransfer = event;
		cy.wrap(subject).trigger('drop', event);
	});
}

// Code stolen from @nrutman here: https://github.com/cypress-io/cypress/issues/170
function b64toBlob(b64Data, contentType = '', sliceSize = 512) {
	const byteCharacters = atob(b64Data);
	const byteArrays = [];

	for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
		const slice = byteCharacters.slice(offset, offset + sliceSize);

		const byteNumbers = new Array(slice.length);
		for (let i = 0; i < slice.length; i++) {
			byteNumbers[i] = slice.charCodeAt(i);
		}

		const byteArray = new Uint8Array(byteNumbers);

		byteArrays.push(byteArray);
	}

	const blob = new Blob(byteArrays, { type: contentType });
	return blob;
}
