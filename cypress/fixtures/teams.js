const Chance = require('chance');
const chance = new Chance();

const main = generateUser(new Date());
module.exports = { main };

function generateUser(date) {
	const stamp = Number(date || new Date()).toString(36);
	const name = `Team:AUI ${stamp}`;

	return {
		description: chance.sentence(),
		name,
		stamp,
		token: stamp,
	};
}
