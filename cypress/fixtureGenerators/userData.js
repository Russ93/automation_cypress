const Chance = require('chance');
const _ = require('lodash');
const chance = new Chance();

module.exports = {
	friend: generateUser(),
	main: generateUser(),
	organization: generateUser({ chargeover: true }),
	trial: generateUser({ pks: true }),
};

function generateUser(override) {
	const data = override || {};
	const date = data.date || new Date();
	const dateStamp = Number(date).toString(36);
	const randStamp = Math.random().toString(36).substr(2);
	const stamp = randStamp + dateStamp;
	const email = `simplysop-aui-${stamp}@getnada.com`;
	const password = `$AUI${stamp}`;
	const firstname = chance.first();
	const lastname = `${stamp} ${chance.last()}`;
	const subdomain = data.chargeover ? stamp : 'app';

	return {
		date,
		dateStamp,
		email,
		firstname,
		fullname: `${firstname} ${lastname}`,
		lastname,
		name: `${firstname} ${lastname}`,
		password,
		pks: false,
		randStamp,
		stamp,
		token: stamp,

		subscriptionType: { id: '5' },
		paymentType: 'creditcard',
		number: chance.cc() || '4242 4242 4242 4242',
		expdate: `01 / ${new Date().getFullYear() + 1}`,
		postcode: '98109',

		company: chance.company(),
		bill_addr1: chance.address(),
		bill_addr2: '226',
		bill_city: 'Seattle',
		bill_state: 'WA',
		bill_postcode: '98109',
		href: `https://${subdomain}.simplysop.com`,
		...data,
	};
}
