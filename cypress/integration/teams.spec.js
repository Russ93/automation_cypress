import userData from '../fixtures/userData';
import teams from '../fixtures/teams';

describe('Teams', () => {
	it('Should create and edit a team', () => {
		cy.login('friend');
		cy.login('main');

		// Visit teams page
		cy.get('[href="/editor/team/all"]').click();
		cy.get('[name="No Teams"] button, [name="With Teams"] button.btn-primary');

		// Click on the new team button
		cy.get('[name="No Teams"] button, [name="With Teams"] button.btn-primary').click();
		cy.get('[action="/api/team"] [name="name"]').focus().invoke('val', teams.main.name);
		cy.get('[action="/api/team"] [name="description"]')
			.focus()
			.invoke('val', teams.main.description);

		// Search for freind
		cy.get('[action="/api/team"] [name="q"]').focus().invoke('val', userData.friend.email).blur();
		cy.wait(1000);
		cy.get('label[for*="userIds-"]').click();

		// Save team
		cy.get('[action="/api/team"] button').click();
		cy.wait(1000);

		// Expect new team to be created
		cy.get('[name="With Teams"] [name="q"]').focus().invoke('val', teams.main.stamp).blur();
		cy.contains(teams.main.stamp).should('be.visible');
		cy.wait(1000);

		// Add description
		cy.get('[name="Team actions"] > button').last().click();
		cy.get('.collapse.show').contains(userData.friend.token).should('be.visible');
		cy.get('[name="Team description"] p').should('be.visible');
	});

	it('Should create a blank team and add a user to it', () => {
		cy.login('friend');
		cy.login('main');

		// Visit teams page
		cy.get('[href="/editor/team/all"]').click();
		cy.get('[name="No Teams"] button, [name="With Teams"] button.btn-primary');

		// Click on the new team button
		cy.get('[name="No Teams"] button, [name="With Teams"] button.btn-primary').click();
		cy.get('[action="/api/team"] [name="name"]').focus().invoke('val', teams.main.name);
		cy.get('[action="/api/team"] [name="description"]')
			.focus()
			.invoke('val', teams.main.description);
		cy.get('[action="/api/team"] button').click();
		cy.get('[name="Team actions"] > button');

		// Add freind
		cy.visit(`/editor/team/all?q=${teams.main.stamp}`);
		cy.get('[name="Team actions"] > button').first().click();
		cy.get('[action="/api/permission/addUser"] [name="q"]')
			.focus()
			.invoke('val', userData.friend.email)
			.blur();
		cy.wait(1000);

		cy.get('label[for*="userIds-"]').click();
		cy.get('[action="/api/permission/addUser"] button').click();
		cy.wait(1000);
	});
});
