import userData from '../fixtures/userData';
import teams from '../fixtures/teams';
import stringifyCircular from '../utils/stringifyCircular';

describe('SOPs Test', () => {
	it('Should create a new SOP', () => {
		const stamp = Number(new Date()).toString(36);
		const SOPName = `SIMPLYSOP:AUI ${stamp}`;
		cy.login('main');

		cy.visit('/editor/procedure/create');
		cy.get('[name="name"]').focus().invoke('val', SOPName);
		cy.get('[value="text"]').parent('label').click();
		cy.get('[action="/api/procedure"] [type="submit"]').click();

		cy.get('[name="SOP Save"]');
		cy.get('[name="name"]').should('have.attr', 'value').and('include', stamp);
		const addStepSl = '[name="Add step action"] .btn-primary';
		cy.get(addStepSl).click();
		cy.get(addStepSl).click();
		cy.get(addStepSl).click();

		cy.wait(1000);
		cy.get('a[href*="step=2"]').click();

		cy.wait(1000);
		cy.get('[name="procedureItems.2.title"]').clear();
		cy.get('[name="procedureItems.2.title"]').focus().invoke('val', SOPName);
		cy.get('[name="SOP actions"] [type="submit"]').click();
		cy.wait(5 * 1000);

		cy.wait(1000);
		cy.get('[name="SOP details"] a').click();
		cy.get('[name*="step-3"] a').should('to.contain', SOPName);
	});

	it('Should launch the SOP builder', () => {
		const stamp = Number(new Date()).toString(36);
		const SOPName = `SIMPLYSOP:AUI ${stamp}`;
		cy.login('main');

		cy.visit('/editor/procedure/create', {
			onBeforeLoad(win) {
				cy.stub(win, 'open');
			},
		});
		cy.wait(1000);

		cy.get('[name="name"]').focus().invoke('val', SOPName);
		cy.get('[type="radio"][value="video"]').parent('label').click();
		cy.get('[action="/api/procedure"] [type="submit"]').click();
		cy.wait(1000);

		cy.url().should('include', '/app-link');
		cy.window().its('open').should('be.called');
	});

	it('Should show a validation message', () => {
		cy.login('main');

		cy.visit('/editor/procedure/create');
		cy.get('[type="radio"][value="video"]').parent('label').click();
		cy.get('[action="/api/procedure"] [type="submit"]').click();
		cy.get('.invalid-feedback').should('be.visible');
	});

	it.skip('Should attach/upload a video and launch SOP builder', () => {
		const stamp = Number(new Date()).toString(36);
		const SOPName = `SIMPLYSOP:AUI ${stamp}`;
		cy.login('main');

		cy.visit('/editor/procedure/create');
		cy.get('[name="name"]').focus().invoke('val', SOPName);
		cy.get('[value="text"]').parent('label').click();
		cy.get('[action="/api/procedure"] [type="submit"]').click();

		cy.get('[name="SOP Save"]');
		cy.get('[name="name"]').should('have.attr', 'value').and('include', stamp);
		cy.get('[name="Add step action"] .btn-primary').click();
		cy.fixture('files/small.webm', 'base64').then((content) => {
			cy.get('[name="Attach a video section"] [data-cy="dropzone"]').upload(
				content,
				`${stamp}.webm`
			);
		});
		cy.wait(10 * 1000);
		cy.get('[name="procedureItems.0.title"]').clear();
		cy.get('[name="procedureItems.0.title"]').focus().invoke('val', stamp);
		cy.get('[name="SOP actions"] [type="submit"]').click();
		cy.wait(5000);

		cy.get('[name="Video edit actions"] button').click();
		cy.contains('Upload a new video').click();
		cy.fixture('files/small.webm', 'base64').then((content) => {
			cy.get('.modal-body [data-cy="dropzone"]').upload(content, `${stamp}-1.webm`);
		});
		cy.wait(10 * 1000);

		cy.window().then((win) => cy.stub(win, 'open'));
		cy.wait(1000);
		cy.get('[name="Video edit actions"] .dropdown-toggle button').click();
		cy.contains('Rerecord').click();
		cy.contains('Launch').click();
		cy.window().its('open').should('be.called');
	});

	it('Should attempt to launch the SOP builder', () => {
		const stamp = Number(new Date()).toString(36);
		const SOPName = `SIMPLYSOP:AUI ${stamp}`;
		cy.login('main');

		cy.visit('/editor/procedure/create', {
			onBeforeLoad(win) {
				cy.stub(win, 'open');
			},
		});

		cy.get('[name="name"]').focus().invoke('val', SOPName);
		cy.get('[value="text"]').parent('label').click();
		cy.get('[action="/api/procedure"] [type="submit"]').click();

		cy.get('[name="SOP Save"]');
		cy.get('[name="name"]').should('have.attr', 'value').and('include', stamp);
		const $addStepBtn = cy.get('[name="Add step action"] .btn-primary');
		$addStepBtn.click();

		cy.get('[name="Launch SOP builder"] button').click();
		cy.wait(1000);

		cy.get('[name="Launch SOP builder"] button')
			.should('have.attr', 'proc')
			.and('include', 'outsourcerec://');
		cy.window().its('open').should('be.called');
	});

	it('Should invite a friend to an SOP', () => {
		const stamp = Number(new Date()).toString(36);
		const SOPName = `SIMPLYSOP:AUI ${stamp}`;
		cy.login('main');

		cy.visit('/editor/procedure/create');
		cy.get('[name="name"]').focus().invoke('val', SOPName);
		cy.get('[value="text"]').parent('label').click();
		cy.get('[action="/api/procedure"] [type="submit"]').click();

		cy.get('[name="SOP Save"]');
		cy.get('[name="name"]').should('have.attr', 'value').and('include', stamp);
		cy.get('[name="Add step action"] .btn-primary').click();

		cy.get('[name="User permissions"] button').click();
		cy.wait(1000);

		cy.get('[name="individuals"] button').click();
		cy.get('[value*="/editor/invite/"]').invoke('val').then(onInviteLink);
		function onInviteLink(inviteLink) {
			cy.login('friend');
			cy.visit(inviteLink);
			cy.wait(10 * 1000);
		}

		cy.visit(`/v/search?q=${stamp}`);
		cy.wait(1500);
		cy.contains(stamp);
	});

	// Permissions: Team Permission add
	it('Should add a Team to an SOP', () => {
		const stamp = Number(new Date()).toString(36);
		const SOPName = `SIMPLYSOP:AUI ${stamp}`;
		cy.login('friend');
		cy.login('main');

		// Open User Dropdown, then navigate to assets using link.
		cy.visit('/editor/team/all');

		cy.get('button[name="Create Team"]').click();
		cy.get('[action="/api/team"] [name="name"]').focus().invoke('val', teams.main.name);
		cy.get('[action="/api/team"] [name="description"]')
			.focus()
			.invoke('val', teams.main.description);
		cy.get('[action="/api/team"] [name="q"]').focus().invoke('val', userData.friend.token).blur();
		cy.wait(1000);

		// Open team edit name.
		cy.get('label[for*="userIds-"]').click();
		cy.get('[action="/api/team"] button').click();
		cy.wait(1000);

		cy.get('[href="/editor/procedure/all"]').click();
		cy.wait(1000);

		// new sop to add team to.
		cy.visit('/editor/procedure/create');
		cy.wait(1000);
		cy.get('[name="name"]').focus().invoke('val', SOPName);
		cy.get('[value="text"]').parent('label').click();
		cy.get('[action="/api/procedure"] [type="submit"]').click();

		cy.get('[name="SOP Save"]');
		cy.get('[name="name"]').should('have.attr', 'value').and('include', stamp);
		cy.get('[name="Add step action"] .btn-primary').click();

		cy.get('div[name="SOP details"] a').click();
		cy.wait(1000);
		cy.get('[name="Open Permissions"]').click();

		cy.get('div[name="teams"] button').click();
		cy.wait(1000);

		cy.get('[action="/api/procedure/addTeam"] [name="q"]')
			.focus()
			.invoke('val', teams.main.token)
			.blur();
		cy.wait(1000);

		cy.get('form label').contains(teams.main.token).click();
		cy.wait(1000);

		cy.get('button').contains('Add Team').click();
	});

	// Permissions:  individual Permission edit
	it('Should add individual Permission to an SOP then edit it', () => {
		const stamp = Number(new Date()).toString(36);
		const SOPName = `SIMPLYSOP:AUI ${stamp}`;
		cy.login('friend');
		cy.login('main');

		cy.visit('/editor/procedure/create');
		cy.wait(1000);
		cy.get('[name="name"]').focus().invoke('val', SOPName);
		cy.get('[value="text"]').parent('label').click();
		cy.get('[action="/api/procedure"] [type="submit"]').click();

		cy.get('[name="SOP Save"]');
		cy.get('[name="name"]').should('have.attr', 'value').and('include', stamp);
		cy.get('[name="Add step action"] .btn-primary').click();

		cy.get('[name="User permissions"] button').click();
		cy.wait(1000);

		cy.get('[name="individuals"] button').scrollIntoView().click();
		cy.get('[value*="/editor/invite/"]').invoke('val').then(onInviteLink);
		function onInviteLink(inviteLink) {
			cy.login('friend');
			cy.visit(inviteLink);
			cy.wait(2000);
			cy.visit(inviteLink);
			cy.wait(2000);

			cy.login('main');
			cy.visit(inviteLink);
			cy.wait(2000);

			cy.get('[name="User permissions"] .btn-light').click();
			cy.wait(1000);

			const userElement = `[name*="${userData.friend.stamp}"] select`;
			cy.get(userElement).select('editor');
			cy.wait(2000);
			cy.get(userElement).select('view');
		}
	});

	// Permissions: individual Permission edit
	it('Should add individual Permission to an SOP then remove it', () => {
		const stamp = Number(new Date()).toString(36);
		const SOPName = `SIMPLYSOP:AUI ${stamp}`;
		cy.login('friend');
		cy.login('main');
		cy.wait(1000);

		/** @description Creates the SOP */
		cy.visit('/editor/procedure/create');
		cy.wait(1000);
		cy.get('[name="name"]').focus().invoke('val', SOPName);
		cy.get('[value="text"]').parent('label').click();
		cy.get('[action="/api/procedure"] [type="submit"]').click();

		cy.get('[name="SOP Save"]');
		cy.get('[name="name"]').should('have.attr', 'value').and('include', stamp);
		cy.get('[name="Add step action"] .btn-primary').click();
		let sopEditLink;
		cy.url().then((url) => (sopEditLink = url));

		/** @description Opens User permissions */
		cy.get('[name="User permissions"] button').click();
		cy.wait(1000);

		/** @description Opens invite link */
		cy.get('[name="individuals"] button').scrollIntoView().click();
		cy.get('[value*="/editor/invite/"]').invoke('val').then(onInviteLink);
		function onInviteLink(inviteLink) {
			/** @description Logins as friend ans visit invite link */
			cy.login('friend');
			cy.visit(inviteLink);
			cy.wait(2000);
			cy.visit(inviteLink);
			cy.wait(2000);

			/** @description Logins back into main */
			cy.login('main');
			cy.visit(sopEditLink);
			cy.wait(1000);

			/** @description Opens User permissions */
			cy.get('button[name="Open Permissions"]').click();
			cy.wait(1000);

			/** @description Removes the user from the SOP */
			cy.get(`[name*="${userData.friend.stamp}"] [name="Delete"]`).click();
			cy.wait(1000);
			cy.get('[name="Yes, remove this user"]').click();
			cy.wait(1000);
		}
	});
});
