import userData from '../fixtures/userData';

describe('Colleagues', () => {
	it('The colleague links should look proper', () => {
		cy.login('main');

		cy.get('[name="Invite friend"] button').click();
		cy.get('[value*="/public/fid/"]')
			.should('have.attr', 'value')
			.and('match', /\/public\/fid\/.+/);
		cy.get('.btn-tp-close').click();

		cy.visit('/v/friends');
		cy.wait(1000);

		cy.get('[name="Friend actions"] button.btn-primary').click();
		cy.get('[value*="/public/fid/"]')
			.should('have.attr', 'value')
			.and('match', /\/public\/fid\/.+/);
	});

	it.skip('The freind link should work properly', () => {
		cy.login('main');

		cy.get('[name="Invite friend"] button').click();

		cy.get('[value*="/public/fid/"]')
			.invoke('val')
			.then((friendLink) => {
				cy.login('friend');

				cy.visit(friendLink);
				cy.wait(2000);

				cy.visit('/v/friends');
				cy.wait(1000);

				/** @description For some reason friends aren't showing up on initial invite */
				cy.get(`[name*="${userData.main.token}"] button[name="details"]`).click();
				cy.get('.MuiDrawer-paper').contains(userData.main.token);
			});
	});
});
