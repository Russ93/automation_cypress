describe('Assets', () => {
	it('Upload an asset using ckfinder', () => {
		const stamp = Number(new Date()).toString(36);
		cy.login('main');

		cy.visit('/user/assets');
		cy.get('[cy-id="ckfinder"] iframe');

		cy.fixture('files/tiny.jpeg', 'base64').then((content) => {
			cy.getIframeBody('[cy-id="ckfinder"] iframe')
				.find('.ui-panel-wrapper')
				.upload(content, `${stamp}.jpeg`);
		});

		cy.getIframeBody('[cy-id="ckfinder"] iframe').contains('Upload finished!', { timeout: 5 * 60 * 1000 });

		cy.visit('/user/assets');
		cy.get('[cy-id="ckfinder"] iframe');

		cy.getIframeBody('[cy-id="ckfinder"] iframe').contains('Files').click();
		cy.getIframeBody('[cy-id="ckfinder"] iframe').contains(stamp).should('be.visible');
	});

	it.skip('Should attach/upload a video and add to the assets manager', () => {
		const stamp = Number(new Date()).toString(36);
		const SOPName = `SIMPLYSOP:AUI ${stamp}`;
		cy.login('main');

		cy.visit('/editor/procedure/create');
		cy.get('[name="name"]').focus().invoke('val', SOPName);
		cy.get('[value="text"]').parent('label').click();
		cy.get('[action="/api/procedure"] [type="submit"]').click();

		cy.get('[name="SOP Save"]');
		cy.get('[name="name"]').should('have.attr', 'value').and('include', stamp);
		cy.get('[name="Add step action"] button').click();
		cy.fixture('files/small.webm', 'base64').then((content) => {
			cy.get('[name="Attach a video section"] [data-cy="dropzone"]').upload(
				content,
				`${stamp}.webm`
			);
		});

		cy.get('[name="Video edit actions"]', { timeout: 5 * 60 * 1000 });
		cy.get('[name="procedureItems.0.title"]').clear();
		cy.get('[name="procedureItems.0.title"]').focus().invoke('val', stamp);
		cy.get('[name="SOP actions"] [type="submit"]').click();
		cy.wait(5000);

		cy.visit('/user/assets');
		cy.wait(1000);

		cy.getIframeBody('[cy-id="ckfinder"] iframe').contains('Files').click();
		cy.getIframeBody('[cy-id="ckfinder"] iframe').contains(stamp).should('be.visible');
	});
});
