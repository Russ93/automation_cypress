import userData from '../fixtures/userData';
import Chance from 'chance';
const chance = new Chance();

describe('User Settings Test', () => {
	it('Should update the users settigns information', () => {
		// const stamp = Number(new Date()).toString(36);
		const firstname = chance.first();

		cy.login('main');
		cy.visit('/user/settings');
		cy.wait(1000);

		cy.get('[name="firstname"]').clear().focus().invoke('val', firstname);
		cy.get('[name="newPassword"]').focus().invoke('val', userData.main.password);
		cy.get('[name="password"]').focus().invoke('val', userData.main.password);
		cy.get('[action*="/api/user/"] [type="submit"]').click();

		cy.wait(1000);
		cy.contains(firstname).should('be.visible');
	});
});
