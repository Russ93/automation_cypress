import userData from '../fixtures/userData';
describe('Registration Test', () => {
	it('Should register a new account and login', () => {
		cy.login('main');

		cy.visit('/user/settings');
		cy.wait(1000);
		cy.contains(userData.main.stamp).should('exist');
	});

	it.skip('Should register a free trial and login', () => {
		const data = userData.trial;

		cy.visit('/public/signup');
		cy.wait(1000);

		cy.get('[name="firstname"]').focus().invoke('val', data.stamp);
		cy.get('[name="lastname"]').focus().invoke('val', data.stamp);
		cy.get('[name="email"]').focus().invoke('val', data.email);
		cy.get('[name="password"]').focus().invoke('val', data.password);
		cy.contains('Continue').click();

		cy.get('[name="Unlimited Account"] button').click();
		cy.wait(3000);

		const iframe = 'iframe[src*="app.paykickstart.com"]';
		cy.getIframeBody(iframe).find('#content-header').should('not.be.visible');
		// cy.getIframeBody(iframe).find('#first_name').should('have.value', data.firstname);
		cy.getIframeBody(iframe).find('#last_name').should('have.value', data.lastname);
		cy.getIframeBody(iframe).find('#email').should('have.value', data.email);
		cy.getIframeBody(iframe).find('#action-button').click();
		cy.wait(10 * 1000);

		cy.getIframeBody(iframe).find('p').should('contain', 'Redirecting...');
		data.registered = true;

		cy.visit('/');
		cy.wait(1000);

		cy.get('[name="username"]').focus().invoke('val', data.email);
		cy.get('[name="password"]').focus().invoke('val', data.password);
		cy.get('[type="submit"]').click();
		cy.wait(1000);

		cy.visit('/user/settings');
		cy.wait(1000);
		cy.contains(userData.main.stamp).should('exist');
	});
});
